package animals;

import java.util.ArrayList;
import java.util.List;

public abstract class Animal {
    int row, col;

    public Animal(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public void move(int row, int col) {
		// FIXME brak walidacji na pola
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

// FIXME Sprawdzanie legalności ruchu brzmi mi raczej jak metoda z planszy a nie z Animal
    public List<int[]> checkMoves(List<int[]> moves, int size, Sheep[] sheeps, Wolf wolf){
        List<int[]> checkedMoves = new ArrayList<>();
        for(int[] move : moves){
            if(move[0] >= 0 && move[0] < size &&	// TODO czym jest move[0] i move[1]? Może 0 i 1 jako stałe, które mają odpowiednią nazwę?
               move[1] >= 0 && move[1] < size){
                boolean isEmpty = true;
                if (wolf.getRow() == move[0] && wolf.getCol() == move[1]) {
                    isEmpty = false;
                }
                for (Sheep sheep : sheeps) {
                    if (sheep.getRow() == move[0] && sheep.getCol() == move[1]) {
                        isEmpty = false;
                    }
                }
                if (isEmpty)
                    checkedMoves.add(move);
            }
        }
        return checkedMoves;
    }
// FIXME dlaczego pojedynczy Animal ma dostawać informacje o wilku i jakiejś większej ilości owiec? Animal powinien mieć dostęp tylko do danych o sobie.
    public abstract List<int[]> possibleMoves(Sheep[] sheeps, Wolf wolf, int size);
}
