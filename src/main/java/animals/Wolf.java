package animals;

import java.util.List;

public class Wolf extends Animal{

    public Wolf(int row, int col) {
        super(row, col);
    }

    @Override
    public List<int[]> possibleMoves(Sheep[] sheeps, Wolf wolf, int size) {
        List<int[]> moves = List.of(new int[] {row+1, col-1}, new int[] {row+1, col+1},
                                    new int[] {row-1, col-1}, new int[] {row-1, col+1});
        return checkMoves(moves, size, sheeps, wolf);
    }
}
