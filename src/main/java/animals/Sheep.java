package animals;

import java.util.ArrayList;
import java.util.List;

public class Sheep extends Animal {


    public Sheep(int row, int col) {
        super(row, col);
    }

    @Override
    public List<int[]> possibleMoves(Sheep[] sheeps, Wolf wolf, int size) {
        List<int[]> moves = List.of(new int[] {row+1, col-1}, new int[] {row+1, col+1});
        return checkMoves(moves, size, sheeps, wolf);
    }
}
