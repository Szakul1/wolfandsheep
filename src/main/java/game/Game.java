package game;

import animals.Sheep;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {

    private final Scanner scanner;

    public Game() {
        scanner = new Scanner(System.in);
    }

    private int getSize() {
        System.out.println("Wybierz rozmiar planszy:");
        int size;
        while (true) { // FIXME while(true) to nie jest dobry pattern. warunek przerwania pętli powinien się znaleźć tutaj, a nie przy break
            boolean flag = !scanner.hasNextInt(); // TODO zmienna powinna mieć nazwę, która mówi co ona robi
            if (flag) { // TODO zmienna 'flag' użyta jest dokładnie raz po przypisaniu, może jest po prostu zbędna
                System.out.println("Niepoprawny rozmiar");

            } else {
                size = scanner.nextInt();
                if (size < 4 || size % 2 != 0) { // TODO Magic number
                    System.out.println("Rozmiar musi być liczbą parzystą >= 4");
                } else {
                    scanner.nextLine();
                    break;
                }
            }
            scanner.nextLine();
        }
        return size;
    }

     void printMoves(List<int[]> moves) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < moves.size(); i++) {
            printMessage(output, i, moves.get(i)[0], moves.get(i)[1]);
        }
        System.out.println(output);
    }

     void printSheeps(List<Sheep> sheep) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < sheep.size(); i++) {
            printMessage(output, i, sheep.get(i).getRow(), sheep.get(i).getCol());
        }
        System.out.println(output);
    }

    void printMessage(StringBuilder output, int index, int row, int col) { // TODO czy ta metoda cokolwiek 'printuje'?
        output.append(index + 1).append(". Wiersz: ").append(row + 1)
                .append(" Kolumna: ").append(col + 1).append("\n");
    }

// TODO co robi ta metoda? czym jest "number", który ona zwraca? Random? konkretny? Nazwa metody powinna więcej mówić
    private int getNumber(int max) { // TODO ta metoda ma bardzo wiele podobieństw z metodą getSize(). Podzielić obie na mniejsze metody, wydzielić część wspólną do osobnej.
        int number;
        while (true) { // FIXME while(true) -> warunek przerwania pętli powinien być w while
            boolean flag = !scanner.hasNextInt();
            if (flag) {
                System.out.println("Niepoprawny numer");
            } else {
                number = scanner.nextInt();
                if (number < 1 || number > max) {
                    System.out.println("Numer musi być z przedziału od 1 do " + max);
                } else {
                    scanner.nextLine();
                    break;
                }
            }
            scanner.nextLine();
        }
        return number - 1;
    }

    public void run() {
        int size = getSize();
        Board board = new Board(size);
        int result;
        System.out.println(board.print());
        while (true) {	// TODO to jest jedyne miejsce, gdzie EWENTUALNIE może być while(true), choć też powinno być wyeliminowane i warunek wyjścia z pętli tutaj. Jeśli pętla musi się wykonać choć raz, są inne pętle.

			// TODO podzielić na osobne metody
            // management of wolf's moves
            System.out.println("Ruch wilka");
            System.out.println("Wybierz ruch:");
            List<int[]> moves = board.getWolfMoves();
            printMoves(moves);
            int move = getNumber(moves.size());
            board.moveWolf(moves.get(move));
            result = board.isGameFinished();
            System.out.println(board.print());
            if (result != 0)
                break;

            // management of sheeps' moves
            System.out.println("Ruch owiec");
            System.out.println("Wybierz owce (które nie są zablokowane)");

            List<Sheep> sheeps = board.getPossibleSheeps();
            printSheeps(sheeps);
            int sheep = getNumber(sheeps.size());

            System.out.println("Wybierz ruch:");
            moves = board.getSheepMoves(sheeps.get(sheep));
            printMoves(moves);
            move = getNumber(moves.size());
            board.moveSheep(sheeps.get(sheep), moves.get(move));

            System.out.println(board.print());
            result = board.isGameFinished();
            if (result != 0)
                break;
        }

        if (result == 1)
            System.out.println("Wygrał wilk");
        else
            System.out.println("Wygrały owce");

        restartGame();
    }

    private void restartGame() {
        System.out.println("r - aby zagrać ponwnie");
        System.out.println("dowolny inny klawisz - zakończ grę");
        String input = scanner.nextLine();
        if (input.equals("r")) {
            run();
            return;
        }
        scanner.close();
    }

}
