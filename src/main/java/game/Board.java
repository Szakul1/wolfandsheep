package game;

import animals.Sheep;
import animals.Wolf;

import java.util.ArrayList;
import java.util.List;

public class Board {
    public Sheep[] sheeps;
    public Wolf wolf;
    private int size;

    public Board(int size) {
        this.size = size;
        sheeps = new Sheep[size / 2];
        for (int i = 0; i < size / 2; i++) {
            sheeps[i] = new Sheep(0, i * 2 + 1); // TODO tu powinien być komentarz skąd i*2 + 1
        }
        wolf = new Wolf(size - 1, 0);
    }

    public int isGameFinished() {	
        if (wolf.getRow() == 0) {
            return 1;// TODO Magic number -> used once: comment; used more than once: constant
        } else if (wolf.possibleMoves(sheeps, wolf, size).isEmpty()) {
            return 2;// TODO Magic number
        } else if (getPossibleSheeps().isEmpty()) {
            return 1;// TODO Magic number
        }
        return 0;// TODO Magic number
    }

    public List<int[]> getWolfMoves(){
        return wolf.possibleMoves(sheeps, wolf, size);
    }

    public List<int[]> getSheepMoves(Sheep sheep){
        return sheep.possibleMoves(sheeps, wolf, size);
    }

    public String print() { // TODO podzielić na osobne metody do rysowania planszy i osobne do rysowania owiec i wilka
        String reset = "\u001B[0m";
        StringBuilder output = new StringBuilder();
        boolean isEmpty;
        for (int i = 0; i < size; i++) {		// TODO zamiast przechodzić przez każde pole i przy co drugim nie robić nic, może lepiej przejść przez co drugie pole
            for (int j = 0; j < size; j++) { 	// np for (int j = i%2; j < size; j+=2)
                if ((i + j) % 2 != 0) {
                    isEmpty = true;
                    if (wolf.getRow() == i && wolf.getCol() == j) {
                        isEmpty = false;
                        output.append("\u001B[31m\uD83D\uDC3A").append(reset); // wolf emoji + color change to red
                    }
                    for (Sheep sheep : sheeps) {
                        if (sheep.getRow() == i && sheep.getCol() == j) {
                            isEmpty = false;
                            output.append("\u001B[32m\uD83D\uDC11").append(reset); // sheep emoji + color change to green
                        }
                    }
                    if (isEmpty)
                        output.append('\u25A0').append(" "); // black square
                } else {
                    output.append("\u25A1").append(" "); // white square
                }
                output.append(" ");
            }
            output.append("\n");
        }
        return output.toString();
    }

    public void moveWolf(int[] move) {
        wolf.move(move[0], move[1]);
    }

    public void moveSheep(Sheep sheep, int[] move) {
        sheep.move(move[0], move[1]);
    }

    public List<Sheep> getPossibleSheeps() { // TODO nazwa metody nie odzwierciedla tego, co metoda robi
        List<Sheep> sheepCords = new ArrayList<>();
        for (Sheep sheep: sheeps) {
            if (!sheep.possibleMoves(sheeps, wolf, size).isEmpty()) {
                sheepCords.add(sheep);
            }
        }
        return sheepCords;
    }
}
