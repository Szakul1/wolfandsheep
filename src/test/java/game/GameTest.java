package game;

import animals.Sheep;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.List;

public class GameTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Test
    void printMovesTest() {	// FIXME ten test ma sprawdzać co zostaje wypisane, czy sprawdzić czy działa standardowe wyjście? Należałoby sprawdzić, czy to co gra wypisuje jest poprawne, niezależnie od tego gdzie zostało napisane.
        Game game = new Game();
        System.setOut(new PrintStream(outputStreamCaptor));
        List<int[]> moves = List.of(new int[]{1, 2}, new int[]{2, 3});
        game.printMoves(moves);
        assertEquals(outputStreamCaptor.toString().trim(), "1. Wiersz: 2 Kolumna: 3\n" +
                "2. Wiersz: 3 Kolumna: 4");
    }

    @Test
    void printSheepsTest() {
        Game game = new Game();
        System.setOut(new PrintStream(outputStreamCaptor));
        Sheep sheep = new Sheep(0,1);
        List<Sheep> sheeps = List.of(sheep);
        game.printSheeps(sheeps);
        assertEquals(outputStreamCaptor.toString().trim(), "1. Wiersz: 1 Kolumna: 2");
    }
}
