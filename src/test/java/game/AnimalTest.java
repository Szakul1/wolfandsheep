package game;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AnimalTest {
    @Test
    void possibleMovesTest() {	// TODO to powinny być osobne testy
        Board board = new Board(4);
        // starting position sheep move
        List<int[]> sheepMoves = board.sheeps[0].possibleMoves(board.sheeps, board.wolf, 4);
        List<int[]> moves = List.of(new int[] {1, 0}, new int[] {1, 2});
        for (int i = 0; i < sheepMoves.size(); i++) {
            assertArrayEquals(sheepMoves.get(i), moves.get(i));
        }
        // starting position wolf move
        List<int[]> wolfMoves = board.getWolfMoves();
        moves = List.of(new int[] {2, 1});
        for (int i = 0; i < wolfMoves.size(); i++) {
            assertArrayEquals(wolfMoves.get(i), moves.get(i));
        }
        // wolf is blocked
        board.sheeps[0].move(2,1);
        assertTrue(board.getWolfMoves().isEmpty());

        //sheep is blocked
        board.sheeps[0].move(1, 2);
        assertTrue(board.getSheepMoves(board.sheeps[1]).isEmpty());
    }
}
