package game;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void finishedGameTest() {	// TODO podzielić na osobne testy
        Board board = new Board(4);	// FIXME ta linijka powtarza się we wszystkich testach w tej klasie. Należy zrobić osobną metodę setup i dodać odpowiednią adnotację @BeforeEach
        assertEquals(0, board.isGameFinished());

        // wolf wins
        // wolf's position = 0
        board.moveSheep(board.sheeps[0], new int[] {3, 2});
        board.moveWolf(new int[] {0, 1});
        assertEquals(1, board.isGameFinished());
        // all sheep blocked
        board.moveWolf(new int[] {1, 2});
        assertEquals(1, board.isGameFinished());

        // sheep wins
        board.moveWolf(new int[] {3, 0});
        board.moveSheep(board.sheeps[0], new int[] {2, 1});
        assertEquals(2, board.isGameFinished());
    }

    @Test
    void animalLocationTest() {
        Board board = new Board(4);
        // wolf position
        assertEquals(board.wolf.getRow(), 3);
        assertEquals(board.wolf.getCol(), 0);

        // sheep positions
        assertEquals(board.sheeps[0].getRow(), 0);
        assertEquals(board.sheeps[0].getCol(), 1);

        assertEquals(board.sheeps[1].getRow(), 0);
        assertEquals(board.sheeps[1].getCol(), 3);
    }

    @Test
    void printTest() {
        Board board = new Board(4);
        String output = "□  \u001B[32m\uD83D\uDC11\u001B[0m □  \u001B[32m\uD83D\uDC11\u001B[0m \n" +
                "■  □  ■  □  \n" +
                "□  ■  □  ■  \n" +
                "\u001B[31m\uD83D\uDC3A\u001B[0m □  ■  □  \n";
        assertEquals(output, board.print());
    }

}